#/***************************************************************************
# CADDigitize
#
#  CAD like tools for QGis
# Fork of Rectangles Ovals Digitizing. Inspired by CadTools, LibreCAD/AutoCAD.
#							 -------------------
#		begin				: 2016-01-25
#		git sha				: $Format:%H$
#		copyright			: (C) 2016 by Loïc BARTOLETTI
#		email				: l.bartoletti@free.fr
# ***************************************************************************/
#
#/***************************************************************************
# *																		 *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or	 *
# *   (at your option) any later version.								   *
# *																		 *
# ***************************************************************************/

#################################################
# Edit the following to match your sources lists
#################################################


#Add iso code for any locales you want to support here (space separated)
# default is no locales
# LOCALES = af
LOCALES = CADDigitize_fr CADDigitize_pt

# If locales are enabled, set the name of the lrelease binary on your system. If
# you have trouble compiling the translations, you may have to specify the full path to
# lrelease
#LRELEASE = lrelease
LRELEASE = lrelease-qt4
LUPDATE = lupdate-qt4

PLUGINNAME = CADDigitize

CADDIGITIZE_ND = \
	CADDigitize_ND/__init__.py \
	CADDigitize_ND/CADDigitize_ND.py \
	CADDigitize_ND/CADDigitize_ND_functions.py \
	CADDigitize_ND/ui_CADDigitize_ND.py

CADCOMMAND = \
      CADCommand/__init__.py \
      CADCommand/CADCommand.py

CADSHAPE = \
	CADShape/__init__.py \
	CADShape/CADCircle.py \
	CADShape/CADCircularArc.py \
	CADShape/CADEllipse.py \
	CADShape/CADRectangle.py \
	CADShape/CADRegularPolygon.py \
	CADShape/CADTriangle.py \
	CADShape/CADUtils.py \
	CADShape/CADPoint.py \
	CADShape/CADLine.py

CADTOOLSMAP = \
	CADToolsMap/__init__.py \
	CADToolsMap/rubber.py \
	CADToolsMap/CADDigitize_tools.py \
	CADToolsMap/CADDigitize_tools_arc.py \
	CADToolsMap/CADDigitize_tools_circle.py \
	CADToolsMap/CADDigitize_tools_ellipse.py \
	CADToolsMap/CADDigitize_tools_rect.py \
	CADToolsMap/CADDigitize_tools_regularpolygon.py \
	CADToolsMap/CADDigitize_tools_modify.py \
	CADToolsMap/CADDigitize_tools_test.py \
	CADToolsMap/CADOptionsToolbar/__init__.py \
	CADToolsMap/CADOptionsToolbar/CADOptionsToolbar.py \
	CADToolsMap/CADOptionsToolbar/CADOptionsArc.py \
	CADToolsMap/CADOptionsToolbar/CADOptionsCircle.py \
	CADToolsMap/CADOptionsToolbar/CADOptionsEllipse.py \
	CADToolsMap/CADOptionsToolbar/CADOptionsModify.py \
	CADToolsMap/CADOptionsToolbar/CADOptionsRect.py \
	CADToolsMap/CADOptionsToolbar/CADOptionsRegularPolygon.py

PY_FILES = \
	__init__.py \
	CADDigitize.py CADDigitize_dockwidget.py CADDigitize_feature.py \
	ui_getAngle.py ui_getDistance.py ui_CADDigitizeSettings.py \
	CADDigitize_dialog.py CADDigitize_tr.py

UI_FILES = CADDigitize_dockwidget_base.ui


ICONS =	\
	icons/arcBy3Points.svg \
	icons/arcByCenter2Points.svg \
	icons/arcByCenterPointAngle.svg \
	icons/circleBy2Points.svg \
	icons/circleBy2Tangents.svg \
	icons/circleBy3Points.svg \
	icons/circleByCenterPoint.svg \
	icons/ellipseBy4Points.svg \
	icons/ellipseByCenter2Points.svg \
	icons/ellipseByCenter3Points.svg \
	icons/ellipseByExtent.svg \
	icons/ellipseByFociPoint.svg \
	icons/ellipseFromCenter.svg \
	icons/modifyBevel.svg \
	icons/modifyFillet.svg \
	icons/modifyOffset.svg \
	icons/modifyRotation.svg \
	icons/modifyExtend.svg \
	icons/modifyTrim.svg \
	icons/rectBy3Points.svg \
	icons/rectByExtent.svg \
	icons/rectFromCenter.svg \
	icons/rpolygonBy2Corners.svg \
	icons/rpolygonByCenterPoint.svg \
	icons/squareFromCenter.svg

EXTRAS = metadata.txt icon.svg

COMPILED_RESOURCE_FILES = resources.py

PEP8EXCLUDE=pydev,resources.py,conf.py,third_party,ui


#################################################
# Normally you would not need to edit below here
#################################################

HELP = help/build/html

PLUGIN_UPLOAD = $(c)/plugin_upload.py

RESOURCE_SRC=$(shell grep '^ *<file' resources.qrc | sed 's@</file>@@g;s/.*>//g' | tr '\n' ' ')

QGISDIR=.qgis2

default: compile

compile: $(COMPILED_RESOURCE_FILES)

%.py : %.qrc $(RESOURCES_SRC)
	pyrcc4 -o $*.py  $<

%.qm : %.ts
	$(LRELEASE) $<

test: compile transcompile
	@echo
	@echo "----------------------"
	@echo "Regression Test Suite"
	@echo "----------------------"

	@# Preceding dash means that make will continue in case of errors
	@-export PYTHONPATH=`pwd`:$(PYTHONPATH); \
		export QGIS_DEBUG=0; \
		export QGIS_LOG_FILE=/dev/null; \
		nosetests -v --with-id --with-coverage --cover-package=. \
		3>&1 1>&2 2>&3 3>&- || true
	@echo "----------------------"
	@echo "If you get a 'no module named qgis.core error, try sourcing"
	@echo "the helper script we have provided first then run make test."
	@echo "e.g. source run-env-linux.sh <path to qgis install>; make test"
	@echo "----------------------"

deploy: compile doc transcompile
	@echo
	@echo "------------------------------------------"
	@echo "Deploying plugin to your .qgis2 directory."
	@echo "------------------------------------------"
	# The deploy  target only works on unix like operating system where
	# the Python plugin directory is located at:
	# $HOME/$(QGISDIR)/python/plugins
	mkdir -p $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vfr CADDigitize_ND $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vfr CADShape $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vfr CADToolsMap $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vfr CADCommand $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vfr icons $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(PY_FILES) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(UI_FILES) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(COMPILED_RESOURCE_FILES) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(EXTRAS) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vfr i18n $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vfr $(HELP) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)/help

# The dclean target removes compiled python files from plugin directory
# also deletes any .git entry
dclean:
	@echo
	@echo "-----------------------------------"
	@echo "Removing any compiled python files."
	@echo "-----------------------------------"
	find $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME) -iname "*.pyc" -delete
	find $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME) -iname ".git" -prune -exec rm -Rf {} \;


derase:
	@echo
	@echo "-------------------------"
	@echo "Removing deployed plugin."
	@echo "-------------------------"
	rm -Rf $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)

zip: deploy dclean
	@echo
	@echo "---------------------------"
	@echo "Creating plugin zip bundle."
	@echo "---------------------------"
	# The zip target deploys the plugin and creates a zip file with the deployed
	# content. You can then upload the zip file on http://plugins.qgis.org
	rm -f $(CURDIR)/pkg/$(PLUGINNAME)-0.2b.zip
	cd $(HOME)/$(QGISDIR)/python/plugins; 7z a -tzip $(CURDIR)/pkg/$(PLUGINNAME)-0.2b.zip $(PLUGINNAME)

package: compile
	# Create a zip package of the plugin named $(PLUGINNAME).zip.
	# This requires use of git (your plugin development directory must be a
	# git repository).
	# To use, pass a valid commit or tag as follows:
	#   make package VERSION=Version_0.3.2
	@echo
	@echo "------------------------------------"
	@echo "Exporting plugin to zip package.	"
	@echo "------------------------------------"
	rm -f $(PLUGINNAME).zip
	git archive --prefix=$(PLUGINNAME)/ -o $(PLUGINNAME).zip $(VERSION)
	echo "Created package: $(PLUGINNAME).zip"

upload: zip
	@echo
	@echo "-------------------------------------"
	@echo "Uploading plugin to QGIS Plugin repo."
	@echo "-------------------------------------"
	$(PLUGIN_UPLOAD) $(PLUGINNAME).zip

transup:
	@echo
	@echo "------------------------------------------------"
	@echo "Updating translation files with any new strings."
	@echo "------------------------------------------------"
	$(LUPDATE) i18n/CADDigitize.pro

transcompile:
	@echo
	@echo "----------------------------------------"
	@echo "Compiled translation files to .qm files."
	@echo "----------------------------------------"
	@chmod +x scripts/compile-strings.sh
	@scripts/compile-strings.sh $(LRELEASE) $(LOCALES)

transclean:
	@echo
	@echo "------------------------------------"
	@echo "Removing compiled translation files."
	@echo "------------------------------------"
	rm -f i18n/*.qm

clean:
	@echo
	@echo "------------------------------------"
	@echo "Removing uic and rcc generated files"
	@echo "------------------------------------"
	rm $(COMPILED_UI_FILES) $(COMPILED_RESOURCE_FILES)

doc:
	@echo
	@echo "------------------------------------"
	@echo "Building documentation using sphinx."
	@echo "------------------------------------"
	cd help; make html

pylint:
	@echo
	@echo "-----------------"
	@echo "Pylint violations"
	@echo "-----------------"
	@pylint --reports=n --rcfile=pylintrc . || true
	@echo
	@echo "----------------------"
	@echo "If you get a 'no module named qgis.core' error, try sourcing"
	@echo "the helper script we have provided first then run make pylint."
	@echo "e.g. source run-env-linux.sh <path to qgis install>; make pylint"
	@echo "----------------------"


# Run pep8 style checking
#http://pypi.python.org/pypi/pep8
pep8:
	@echo
	@echo "-----------"
	@echo "PEP8 issues"
	@echo "-----------"
	@pep8 --repeat --ignore=E203,E121,E122,E123,E124,E125,E126,E127,E128 --exclude $(PEP8EXCLUDE) . || true
	@echo "-----------"
	@echo "Ignored in PEP8 check:"
	@echo $(PEP8EXCLUDE)
