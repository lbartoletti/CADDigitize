#!/usr/bin/env bash
LOCALES=$*

echo "Please provide translations by editing the translation files below:"
for LOCALE in ${LOCALES}
do
    echo "i18n/"${LOCALE}".ts"
    # Note we don't use pylupdate with qt .pro file approach as it is flakey
    # about what is made available.
    pylupdate4 `find . -regex ".*py" -type f` `find . -regex ".*ui" -type f` -noobsolete -ts i18n/${LOCALE}.ts
done
